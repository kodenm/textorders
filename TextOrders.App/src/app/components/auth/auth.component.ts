import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { TokenService } from 'src/app/services/token.service';

const loginFormGroup: FormGroup = new FormGroup({
  "login": new FormControl('', [Validators.required, Validators.minLength(8)]),
  "password": new FormControl('', [Validators.required, Validators.minLength(8)])
});

const signupFormGroup: FormGroup = new FormGroup({
  "login": new FormControl(undefined, [Validators.required, Validators.minLength(8)]),
  "email": new FormControl(undefined, [Validators.required, Validators.email]),
  "password": new FormControl(undefined, [Validators.required, Validators.minLength(8)]),
  "name": new FormGroup({
    "firstName": new FormControl(undefined, Validators.required),
    "lastName": new FormControl(undefined, Validators.required)
  }),
  "type": new FormControl(undefined, Validators.required)
});

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {
  isLoginPage: boolean;
  formGroup: FormGroup;
  options = [
    'Client',
    'Writer'
  ];
  errorMessage?: string;

  constructor(private authService: AuthService, private tokenService: TokenService, private router: Router) {
    if (authService.isLogged()) {
      this.router.navigateByUrl('/home');
    }

    this.isLoginPage = true;
    this.formGroup = new FormGroup({});
    this.errorMessage = undefined;
  }

  ngOnInit(): void {
    this.loadFormGroup();
    this.formGroup.reset();
  }

  private loadFormGroup() {
    if (this.isLoginPage) {
      this.formGroup = loginFormGroup;
    }
    else {
      this.formGroup = signupFormGroup;
    }
  }

  changeState() {
    this.isLoginPage = !this.isLoginPage;
    this.loadFormGroup();
    this.formGroup.reset();
  }

  submit() {
    if (!this.formGroup.valid) {
      this.errorMessage = 'Form contains invalid data';
      return;
    }

    console.log(JSON.stringify(this.formGroup.value));

    const onSuccessfullLogin = (response: any) => {
      console.log('successfully logged in');
      this.tokenService.setToken(response.token);
      this.router.navigateByUrl('/home');
    };

    const onFailedLogin = (response: any) => {
      this.errorMessage = response.error;
      console.log(response.error);
    }

    if (this.isLoginPage) {
      this.authService.login(this.formGroup.controls['login'].value, this.formGroup.controls['password'].value).subscribe(onSuccessfullLogin, onFailedLogin);
    }
    else {
      this.authService.signup(this.formGroup.value).subscribe((response: any) => {
        this.authService.login(this.formGroup.controls['login'].value, this.formGroup.controls['password'].value).subscribe(onSuccessfullLogin);
      }, onFailedLogin);
    }
  }
}
