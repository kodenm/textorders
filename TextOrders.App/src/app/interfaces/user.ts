import { UserType } from "../enums/user-type";

export interface User {
    id: string,
    login: string,
    email: string,
    password: string,
    name: {
        firstName: string,
        lastName: string
    },
    type: UserType,
    registeredAt: Date
}
