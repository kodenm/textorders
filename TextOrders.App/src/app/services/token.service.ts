import { Injectable } from '@angular/core';
import decode from 'jwt-decode'

@Injectable({
  providedIn: 'root'
})
export class TokenService {
  token?: string;


  constructor() {
  }

  setToken(token: string) {
    localStorage.setItem('token', token);
  }

  removeToken() {
    localStorage.removeItem('token');
  }

  getToken() {
    return localStorage.getItem('token');
  }

  getPayload(): any {
    const token = localStorage.getItem('token') ?? undefined;
    if (!token) {
      return null;
    }
    const tokenPayload = decode(token);
    return tokenPayload;
  }

  isAuthenticated() {
    return !!localStorage.getItem('token');
  }
}
