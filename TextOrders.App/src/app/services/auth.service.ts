import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../interfaces/user';
import { TokenService } from './token.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(private tokenService: TokenService, private httpClient: HttpClient) {
  }

  login(login: string, password: string) {
    return this.httpClient.post('http://localhost/TextOrders.Data.Service/api/auth', { login: login, password: password });
    // .subscribe((response: any) => {
    //   this.isLogged = true;
    //   this.tokenService.setToken(response.token);
    //   this.router.navigateByUrl('/home');
    //   console.log("successfully logged in");
    // },
    // error => {
    //   this.isLogged = false;
    //   this.errorMessage = error.error;
    //   console.log(error.error);
    // });
  }

  signup(user: User) {
    return this.httpClient.post('http://localhost/TextOrders.Data.Service/api/user', user);
    // .subscribe((response: any) => {
    //   console.log("successfully signed up");
    //   this.login(user.login, user.password);
    // },
    // error => {
    //   this.errorMessage = error.error;
    //   console.log(error.error);
    // });
  }

  logout() {
    this.tokenService.removeToken();
  }

  isLogged() {
    return this.tokenService.isAuthenticated();
  }
}
