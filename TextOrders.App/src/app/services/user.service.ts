import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from '../interfaces/user';
import { TokenService } from './token.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  constructor(private httpClient: HttpClient, private tokenService: TokenService) {

  }

  getUser() {
    return this.httpClient.get<User>(`http://localhost/TextOrders.Data.Service/api/user?id=${this.tokenService.getPayload().id}`);
  }
}
