﻿using ModelUser = TextOrders.Core.Models.User;
using EfUser = TextOrders.Data.Infrastructure.Models.User;

namespace TextOrders.Data.Infrastructure.Extensions
{
    public static class UserExtensions
    {
        public static void UpdateFromModel(this EfUser efUser, ModelUser modelUser)
        {
            efUser.Login = string.IsNullOrEmpty(modelUser.Login) ? efUser.Login : modelUser.Login;
            efUser.Email = string.IsNullOrEmpty(modelUser.Email) ? efUser.Email : modelUser.Email;
            efUser.PasswordHash = string.IsNullOrEmpty(modelUser.Password) ? efUser.PasswordHash : modelUser.Password;
            efUser.FirstName = string.IsNullOrEmpty(modelUser.Name.FirstName) ? efUser.FirstName : modelUser.Name.FirstName;
            efUser.LastName = string.IsNullOrEmpty(modelUser.Name.LastName) ? efUser.LastName : modelUser.Name.LastName;
        }
    }
}
