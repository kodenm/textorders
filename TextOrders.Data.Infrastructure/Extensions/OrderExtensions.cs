﻿using EfOrder = TextOrders.Data.Infrastructure.Models.Order;
using ModelOrder = TextOrders.Core.Models.Order;

namespace TextOrders.Data.Infrastructure.Extensions
{
    public static class OrderExtensions
    {
        // CONSIDER: Updating WriterId must be forbidden for a user.
        public static void UpdateFromModel(this EfOrder efOrder, ModelOrder modelOrder)
        {
            efOrder.Subject = string.IsNullOrEmpty(modelOrder.Subject) ? efOrder.Subject : modelOrder.Subject;
            efOrder.Description = modelOrder.Description;
            efOrder.Text = modelOrder.Text;
            efOrder.WriterId = modelOrder.WriterId;
            efOrder.Status = modelOrder.Status.ToString();
            efOrder.BeganAt = modelOrder.BeganAt;
            efOrder.FinishedAt = modelOrder.FinishedAt;
        }
    }
}
