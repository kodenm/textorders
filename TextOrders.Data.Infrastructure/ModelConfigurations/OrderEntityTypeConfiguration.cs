﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TextOrders.Data.Infrastructure.Models;

namespace TextOrders.Data.Infrastructure.ModelConfigurations
{
    public class OrderEntityTypeConfiguration : IEntityTypeConfiguration<Order>
    {
        public void Configure(EntityTypeBuilder<Order> builder)
        {
            builder.ToTable("Order");

            builder.HasKey(o => o.Id);
            builder.Property(o => o.Id)
                .HasColumnType("uuid")
                .HasDefaultValueSql("uuid_generate_v4()")
                .IsRequired();
            builder.Property(o => o.Subject).IsRequired();
            builder.Property(o => o.Status).IsRequired();
            builder.HasOne(o => o.Client).WithMany(u => u.RequestedOrders).HasForeignKey(o => o.ClientId).OnDelete(DeleteBehavior.SetNull);
            builder.HasOne(o => o.Writer).WithMany(u => u.CompletedOrders).HasForeignKey(o => o.WriterId).OnDelete(DeleteBehavior.SetNull);
        }
    }
}
