﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using TextOrders.Data.Infrastructure.ModelConfigurations;
using TextOrders.Data.Infrastructure.Models;

namespace TextOrders.Data.Infrastructure
{
    public class DataContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Order> Orders { get; set; }

        public DataContext(DbContextOptions options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasPostgresExtension("uuid-ossp");

            modelBuilder.ApplyConfiguration(new UserEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new OrderEntityTypeConfiguration());
        }
    }
}
