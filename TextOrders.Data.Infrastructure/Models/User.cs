﻿using System;
using System.Collections.Generic;

namespace TextOrders.Data.Infrastructure.Models
{
    public class User
    {
        public Guid Id { get; set; }
        public string Login { get; set; }
        public string Email { get; set; }
        public string PasswordHash { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Type { get; set; }
        public DateTime RegisteredAt { get; set; }
        public List<Order> RequestedOrders { get; set; }
        public List<Order> CompletedOrders { get; set; }
    }
}
