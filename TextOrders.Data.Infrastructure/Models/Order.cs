﻿using System;

namespace TextOrders.Data.Infrastructure.Models
{
    public class Order
    {
        public Guid Id { get; set; }
        public string Subject { get; set; }
        public string Description { get; set; }
        public string Text { get; set; }
        public Guid? ClientId { get; set; }
        public User Client { get; set; }
        public Guid? WriterId { get; set; }
        public User Writer { get; set; }
        public string Status { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? BeganAt { get; set; }
        public DateTime? FinishedAt { get; set; }
    }
}
