﻿using AutoMapper;
using TextOrders.Core.Models;
using TextOrders.Data.Service.Models;

namespace TextOrders.Data.Service.MapperProfiles
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<CreationUser, User>()
                .ForMember(d => d.Login, opt => opt.MapFrom(s => s.Login))
                .ForMember(d => d.Email, opt => opt.MapFrom(s => s.Email))
                .ForMember(d => d.Password, opt => opt.MapFrom(s => s.Password))
                .ForMember(d => d.Name, opt => opt.MapFrom(s => s.Name))
                .ForMember(d => d.Type, opt => opt.MapFrom(s => (Core.Enums.UserType)s.Type));

            CreateMap<UpdateUser, User>()
                .ForMember(d => d.Login, opt => opt.MapFrom(s => s.Login))
                .ForMember(d => d.Email, opt => opt.MapFrom(s => s.Email))
                .ForMember(d => d.Name, opt => opt.MapFrom(s => s.Name))
                .ForMember(d => d.Password, opt => opt.MapFrom(s => s.Password));

            CreateMap<User, InfoUser>()
                .ForMember(d => d.Id, opt => opt.MapFrom(s => s.Id))
                .ForMember(d => d.Login, opt => opt.MapFrom(s => s.Login))
                .ForMember(d => d.Email, opt => opt.MapFrom(s => s.Email))
                .ForMember(d => d.Name, opt => opt.MapFrom(s => s.Name))
                .ForMember(d => d.Type, opt => opt.MapFrom(s => (Enums.UserType)s.Type))
                .ForMember(d => d.RegisteredAt, opt => opt.MapFrom(s => s.RegisteredAt));

            CreateMap<Core.Models.FullName, Models.FullName>()
                .ForMember(s => s.FirstName, opt => opt.MapFrom(s => s.FirstName))
                .ForMember(s => s.LastName, opt => opt.MapFrom(s => s.LastName));

            CreateMap<Models.FullName, Core.Models.FullName>()
                .ForMember(s => s.FirstName, opt => opt.MapFrom(s => s.FirstName))
                .ForMember(s => s.LastName, opt => opt.MapFrom(s => s.LastName));
        }
    }
}
