﻿using Microsoft.AspNetCore.Http;
using System;
using System.Threading.Tasks;
using TextOrders.Data.Managers.Model;

namespace TextOrders.Data.Service.Middleware
{
    public class ExceptionFilter
    {
        private readonly RequestDelegate _next;

        public ExceptionFilter(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            try
            {
                await _next.Invoke(context);
            }
            catch (DataException ex)
            {
                context.Response.StatusCode = StatusCodes.Status400BadRequest;
                await context.Response.WriteAsJsonAsync(ex.Message);
            }
            catch (AccessViolationException ex)
            {
                context.Response.StatusCode = StatusCodes.Status401Unauthorized;
                await context.Response.WriteAsJsonAsync(ex.Message);
            }
            catch (Exception ex)
            {
                context.Response.StatusCode = 500;
                await context.Response.WriteAsJsonAsync(ex.Message);
            }
        }
    }
}
