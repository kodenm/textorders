﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;
using TextOrders.Data.Managers.Model;
using TextOrders.Data.Service.Models;
using TextOrders.Identity.Core.Utilities;

namespace TextOrders.Data.Service.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces("application/json")]
    [AllowAnonymous]
    public class AuthController : Controller
    {
        private readonly IConfiguration _configuration;
        private readonly IUserService _userService;
        public AuthController(IConfiguration configuration, IUserService userService)
        {
            _configuration = configuration;
            _userService = userService;
        }

        [HttpPost]
        public async Task<ActionResult> LoginAsync(LoginForm loginForm)
        {
            if (string.IsNullOrEmpty(loginForm.Login) || string.IsNullOrEmpty(loginForm.Password))
            {
                return BadRequest("Credentials must not be empty");
            }

            var user = await _userService.GetAsync(u => u.Login == loginForm.Login || u.Email == loginForm.Login);
            if (PasswordHasher.Verify(loginForm.Password, user.Password))
            {
                return Ok(new { token = JwtManager.Generate(user, _configuration) });
            }
            else
            {
                return BadRequest("Credentials are invalid");
            }
        }
    }
}
