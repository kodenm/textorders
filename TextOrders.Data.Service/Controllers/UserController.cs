﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;
using TextOrders.Core.Models;
using TextOrders.Data.Managers.Model;
using TextOrders.Data.Service.Models;

namespace TextOrders.Data.Service.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    [Produces("application/json")]
    public class UserController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IUserService _userService;

        public UserController(IMapper mapper, IUserService userService)
        {
            _mapper = mapper;
            _userService = userService;
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> AddAsync(CreationUser user)
        {
            await _userService.AddAsync(_mapper.Map<User>(user));

            return Ok();
        }

        [HttpGet]
        public async Task<ActionResult> GetAsync(Guid id)
        {
            if (!HasAuthorizedAccess(id))
            {
                return Unauthorized();
            }

            return Ok(_mapper.Map<InfoUser>(await _userService.GetAsync(id)));
        }

        [HttpPut]
        public async Task<ActionResult> UpdateAsync(Guid id, UpdateUser user)
        {
            if (!HasAuthorizedAccess(id))
            {
                return Unauthorized();
            }

            await _userService.UpdateAsync(id, _mapper.Map<User>(user));

            return Ok();
        }

        [HttpDelete]
        public async Task<ActionResult> DeleteAsync(Guid id)
        {
            if (!HasAuthorizedAccess(id))
            {
                return Unauthorized();
            }

            await _userService.DeleteAsync(id);

            return Ok();
        }

        private bool HasAuthorizedAccess(Guid id)
        {
            return HttpContext.User.Claims.First(c => c.Type == "id").Value == id.ToString();
        }
    }
}
