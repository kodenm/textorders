﻿using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TextOrders.Core.Models;
using TextOrders.Data.Managers.Model;
using TextOrders.Data.Service.Models;

namespace TextOrders.Data.Service.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    [Produces("application/json")]
    public class OrderController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IOrderService _orderService;
        private readonly IUserService _userService;

        public OrderController(IMapper mapper, IOrderService orderService, IUserService userService)
        {
            _mapper = mapper;
            _orderService = orderService;
            _userService = userService;
        }

        [HttpPost]
        public async Task<ActionResult> AddAsync(CreationOrder creationOrder)
        {
            var order = _mapper.Map<Order>(creationOrder);
            if (!HasAuthorizedAccess(order.ClientId))
            {
                return Unauthorized();
            }

            await _orderService.AddAsync(order);
            
            return Ok();
        }

        [HttpGet("list")]
        public async Task<ActionResult> GetAsync()
        {
            // TODO: make order service method that returns a list of all orders
            return Ok(await _orderService.GetAsync(u => true));
        }

        [HttpGet]
        public async Task<ActionResult> GetAsync([FromQuery] Guid id)
        {
            var order = await _orderService.GetAsync(id);
            if (!HasAuthorizedAccess(order.ClientId) || order.WriterId != null && !HasAuthorizedAccess(order.WriterId.Value))
            {
                return Unauthorized();
            }

            return Ok(_mapper.Map<InfoOrder>(await _orderService.GetAsync(id)));
        }

        [HttpPost("job")]
        public async Task<ActionResult> ProceedAsync(Guid orderId, Guid writerId)
        {
            var order = await _orderService.GetAsync(orderId);
            if (order.WriterId == null && !HasAuthorizedAccess(writerId))
            {
                return Unauthorized();
            }

            await _orderService.ProceedAsync(orderId, writerId);

            return Ok();
        }

        [HttpPost("review")]
        public async Task<ActionResult> ReviewAsync(Guid id)
        {
            var order = await _orderService.GetAsync(id);
            if (order.WriterId != null && !HasAuthorizedAccess(order.WriterId.Value))
            {
                return Unauthorized();
            }

            await _orderService.ReviewAsync(id);

            return Ok();
        }

        [HttpPost("completion")]
        public async Task<ActionResult> CompleteAsync(Guid id)
        {
            var order = await _orderService.GetAsync(id);
            if (!HasAuthorizedAccess(order.ClientId))
            {
                return Unauthorized("Order not found");
            }
            await _orderService.CompleteAsync(id);

            return Ok();
        }

        [HttpPost("publishment")]
        public async Task<ActionResult> CompleteAsync(Guid id, [FromBody] string text)
        {
            var order = await _orderService.GetAsync(id);
            if (order.WriterId != null && !HasAuthorizedAccess(order.WriterId.Value))
            {
                return Unauthorized();
            }

            await _orderService.PublishTextAsync(id, text);

            return Ok();
        }

        private bool HasAuthorizedAccess(Guid userId)
        {
            return HttpContext.User.Claims.First(c => c.Type == "id").Value == userId.ToString();
        }
    }
}
