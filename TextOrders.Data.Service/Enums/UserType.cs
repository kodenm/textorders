﻿namespace TextOrders.Data.Service.Enums
{
    public enum UserType
    {
        Writer,
        Client
    }
}
