﻿namespace TextOrders.Data.Service.Enums
{
    public enum OrderStatus
    {
        Awaiting,
        InProgress,
        OnReview,
        Completed,
        Closed
    }
}
