﻿using Newtonsoft.Json;

namespace TextOrders.Data.Service.Models
{
    public class LoginForm
    {
        [JsonProperty(Required = Required.Always)]
        public string Login { get; set; }

        [JsonProperty(Required = Required.Always)]
        public string Password { get; set; }
    }
}
