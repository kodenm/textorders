﻿using Newtonsoft.Json;
using System;

namespace TextOrders.Data.Service.Models
{
    public class CreationOrder
    {
        [JsonProperty(Required = Required.Always)]
        public string Subject { get; set; }
        [JsonProperty(Required = Required.Always)]
        public string Description { get; set; }
        [JsonProperty(Required = Required.Always)]
        public Guid? ClientId { get; set; }

    }
}
