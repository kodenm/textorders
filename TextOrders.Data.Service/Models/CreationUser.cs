﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using TextOrders.Core.Validators;
using TextOrders.Data.Service.Enums;

namespace TextOrders.Data.Service.Models
{
    [JsonObject(MemberSerialization = MemberSerialization.OptOut, MissingMemberHandling = MissingMemberHandling.Error)]
    public class CreationUser
    {
        [JsonProperty(Required = Required.Always)]
        [RegularExpression("^[a-zA-Z0-9_]+$", ErrorMessage = "Login contains forbidden symbols")]
        public string Login { get; set; }

        [JsonProperty(Required = Required.Always)]
        [RegularExpression(@"^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-‌​]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$", ErrorMessage = "Email is not valid")]
        public string Email { get; set; }

        [JsonProperty(Required = Required.Always)]
        [MinLength(8, ErrorMessage = "Password must be minimum of 8 symbols")]
        public string Password { get; set; }

        [JsonProperty(Required = Required.Always)]
        [Name("Name contains forbidden symbols")]
        public FullName Name { get; set; }

        [EnumDataType(typeof(UserType))]
        [JsonProperty(Required = Required.Always)]
        public UserType Type { get; set; }
    }
}
