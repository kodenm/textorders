﻿using System.ComponentModel.DataAnnotations;

namespace TextOrders.Data.Service.Models
{
    public class FullName
    {
        [RegularExpression("^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$", ErrorMessage = "First name contains forbidden symbols")]
        public string FirstName { get; set; }

        [RegularExpression("^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$", ErrorMessage = "Last name contains forbidden symbols")]
        public string LastName { get; set; }
    }
}
