﻿using System;
using TextOrders.Data.Service.Enums;

namespace TextOrders.Data.Service.Models
{
    public class InfoUser
    {
        public Guid Id { get; set; }
        public string Login { get; set; }
        public string Email { get; set; }
        public FullName Name { get; set; }
        public UserType Type { get; set; }
        public DateTime RegisteredAt { get; set; }
    }
}
