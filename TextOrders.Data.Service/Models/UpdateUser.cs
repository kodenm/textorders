﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using TextOrders.Core.Validators;

namespace TextOrders.Data.Service.Models
{
    public class UpdateUser
    {
        [RegularExpression("^[a-zA-Z0-9_]+$", ErrorMessage = "Login contains forbidden symbols")]
        public string Login { get; set; }
        [RegularExpression(@"^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-‌​]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$", ErrorMessage = "Email is not valid")]
        public string Email { get; set; }
        [MinLength(8, ErrorMessage = "Password must be minimum of 8 symbols")]
        public string Password { get; set; }
        public FullName Name { get; set; }
    }
}
