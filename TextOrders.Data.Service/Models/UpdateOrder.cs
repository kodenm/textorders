﻿using Newtonsoft.Json;
using System;
using TextOrders.Data.Service.Enums;

namespace TextOrders.Data.Service.Models
{
    public class UpdateOrder
    {
        [JsonProperty(Required = Required.Always)]
        public string Subject { get; set; }
        [JsonProperty(Required = Required.Always)]
        public string Description { get; set; }
        // CONSIDER: These fields must not be edited by a user
        //public string Text { get; set; }
        //public Guid? WriterId { get; set; }
        //public OrderStatus Status { get; set; }
        //public DateTime? BeganAt { get; set; }
        //public DateTime? FinishedAt { get; set; }
    }
}
