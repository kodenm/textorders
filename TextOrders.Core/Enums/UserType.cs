﻿namespace TextOrders.Core.Enums
{
    public enum UserType
    {
        Writer,
        Client
    }
}
