﻿namespace TextOrders.Core.Enums
{
    public enum OrderStatus
    {
        Awaiting,
        InProgress,
        OnReview,
        Completed
    }
}
