﻿using System;
using TextOrders.Core.Enums;

namespace TextOrders.Core.Models
{
    public class Order
    {
        public Guid Id { get; set; }
        public string Subject { get; set; }
        public string Description { get; set; }
        public string Text { get; set; }
        public Guid ClientId { get; set; }
        public Guid? WriterId { get; set; }
        public OrderStatus Status { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? BeganAt { get; set; }
        public DateTime? FinishedAt { get; set; }
    }
}
