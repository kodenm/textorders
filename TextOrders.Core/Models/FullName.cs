﻿using System.ComponentModel.DataAnnotations;

namespace TextOrders.Core.Models
{
    public class FullName
    {
        [RegularExpression("^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$", ErrorMessage ="First name contains forbidden symbols")]
        public string FirstName { get; set; }

        [RegularExpression("^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$", ErrorMessage = "Last name contains forbidden symbols")]
        public string LastName { get; set; }

        public static void FormatName(FullName fullname)
        {
            fullname.FirstName = char.ToUpper(fullname.FirstName[0]) + fullname.FirstName.Substring(1);
            fullname.LastName = char.ToUpper(fullname.LastName[0]) + fullname.LastName.Substring(1);
        }
    }
}
