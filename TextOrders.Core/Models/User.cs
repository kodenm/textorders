﻿using System;
using System.ComponentModel.DataAnnotations;
using TextOrders.Core.Enums;
using TextOrders.Core.Validators;

namespace TextOrders.Core.Models
{
    public class User
    {
        public Guid Id { get; set; }
        public string Login { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public FullName Name { get; set; }
        public UserType Type { get; set; }
        public DateTime RegisteredAt { get; set; }
    }
}
