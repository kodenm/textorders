﻿using System;
using System.ComponentModel.DataAnnotations;
using TextOrders.Core.Models;

namespace TextOrders.Core.Validators
{
    [AttributeUsage(AttributeTargets.Property)]
    public class NameAttribute : ValidationAttribute
    {
        public NameAttribute()
        {

        }
        public NameAttribute(string errorMessage) : base(errorMessage)
        {

        }

        public override bool IsValid(object value)
        {
            if (value == null)
            {
                return false;
            }
            return Validator.TryValidateObject(value, new ValidationContext(value), null, true);
        }
    }
}
