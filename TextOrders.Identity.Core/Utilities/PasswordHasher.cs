﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace TextOrders.Identity.Core.Utilities
{
    public static class PasswordHasher
    {
        public static string Generate(string password)
        {
            using var sha256 = SHA256.Create();
            var hashBytes = sha256.ComputeHash(Encoding.UTF8.GetBytes(password));
            var passwordHash = BitConverter.ToString(hashBytes).Replace("-", "").ToLower();
            return passwordHash;
        }

        public static bool Verify(string password, string passwordHash)
        {
            return Generate(password) == passwordHash;
        }
    }
}
