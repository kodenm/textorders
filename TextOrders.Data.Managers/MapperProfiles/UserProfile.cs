﻿using AutoMapper;
using System;
using TextOrders.Core.Enums;
using TextOrders.Core.Models;
using DomainUser = TextOrders.Core.Models.User;
using EfUser = TextOrders.Data.Infrastructure.Models.User;

namespace TextOrders.Data.Managers.MapperProfiles
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<DomainUser, EfUser>()
                .ForMember(d => d.Id, opt => opt.MapFrom(s => s.Id))
                .ForMember(d => d.Login, opt => opt.MapFrom(s => s.Login.Trim()))
                .ForMember(d => d.Email, opt => opt.MapFrom(s => s.Email.Trim()))
                .ForMember(d => d.PasswordHash, opt => opt.MapFrom(s => s.Password))
                .ForMember(d => d.FirstName, opt => opt.MapFrom(s => s.Name.FirstName.Trim()))
                .ForMember(d => d.LastName, opt => opt.MapFrom(s => s.Name.LastName.Trim()))
                .ForMember(d => d.Type, opt => opt.MapFrom(s => s.Type.ToString()))
                .ForMember(d => d.RegisteredAt, opt => opt.MapFrom(s => s.RegisteredAt));

            CreateMap<EfUser, DomainUser>()
                .ForMember(d => d.Id, opt => opt.MapFrom(s => s.Id))
                .ForMember(d => d.Login, opt => opt.MapFrom(s => s.Login))
                .ForMember(d => d.Email, opt => opt.MapFrom(s => s.Email))
                .ForMember(d => d.Password, opt => opt.MapFrom(s => s.PasswordHash))
                .ForMember(d => d.Name, opt => opt.MapFrom(s => new FullName { FirstName = s.FirstName, LastName = s.LastName }))
                .ForMember(d => d.Type, opt => opt.MapFrom(s => Enum.Parse<UserType>(s.Type)))
                .ForMember(d => d.RegisteredAt, opt => opt.MapFrom(s => s.RegisteredAt));
        }
    }
}
