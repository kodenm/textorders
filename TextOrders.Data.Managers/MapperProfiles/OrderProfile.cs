﻿using AutoMapper;
using TextOrders.Core.Enums;
using ModelOrder = TextOrders.Core.Models.Order;
using EfOrder = TextOrders.Data.Infrastructure.Models.Order;

namespace TextOrders.Data.Managers.MapperProfiles
{
    public class OrderProfile : Profile
    {
        public OrderProfile()
        {
            CreateMap<ModelOrder, EfOrder>()
                .ForMember(d => d.Id, opt => opt.MapFrom(s => s.Id))
                .ForMember(d => d.Subject, opt => opt.MapFrom(s => s.Subject))
                .ForMember(d => d.Description, opt => opt.MapFrom(s => s.Description))
                .ForMember(d => d.Text, opt => opt.MapFrom(s => s.Text))
                .ForMember(d => d.ClientId, opt => opt.MapFrom(s => s.ClientId))
                .ForMember(d => d.WriterId, opt => opt.MapFrom(s => s.WriterId))
                .ForMember(d => d.Status, opt => opt.MapFrom(s => s.Status.ToString()))
                .ForMember(d => d.CreatedAt, opt => opt.MapFrom(s => s.CreatedAt))
                .ForMember(d => d.BeganAt, opt => opt.MapFrom(s => s.BeganAt))
                .ForMember(d => d.FinishedAt, opt => opt.MapFrom(s => s.FinishedAt));
            
            CreateMap<EfOrder, ModelOrder>()
                .ForMember(d => d.Id, opt => opt.MapFrom(s => s.Id))
                .ForMember(d => d.Subject, opt => opt.MapFrom(s => s.Subject))
                .ForMember(d => d.Description, opt => opt.MapFrom(s => s.Description))
                .ForMember(d => d.Text, opt => opt.MapFrom(s => s.Text))
                .ForMember(d => d.ClientId, opt => opt.MapFrom(s => s.ClientId))
                .ForMember(d => d.WriterId, opt => opt.MapFrom(s => s.WriterId))
                .ForMember(d => d.Status, opt => opt.MapFrom(s => Enum.Parse<OrderStatus>(s.Status)))
                .ForMember(d => d.CreatedAt, opt => opt.MapFrom(s => s.CreatedAt))
                .ForMember(d => d.BeganAt, opt => opt.MapFrom(s => s.BeganAt))
                .ForMember(d => d.FinishedAt, opt => opt.MapFrom(s => s.FinishedAt));
        }
    }
}
