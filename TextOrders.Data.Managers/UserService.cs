﻿using AutoMapper;
using System.Linq.Expressions;
using TextOrders.Core.Models;
using TextOrders.Data.Infrastructure;
using TextOrders.Data.Managers.Model;
using ModelUser = TextOrders.Core.Models.User;
using EfUser = TextOrders.Data.Infrastructure.Models.User;
using TextOrders.Identity.Core.Utilities;
using Microsoft.EntityFrameworkCore;

namespace TextOrders.Data.Managers
{
    public class UserService : IUserService
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;

        public UserService(DataContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public void Add(ModelUser user)
        {
            if (_context.Users.Any(u => u.Login == user.Login || u.Email == user.Email))
            {
                throw new DataException("User with this login or email can't be created");
            }

            user.Login = user.Login.ToLower();
            user.Email = user.Email.ToLower();
            FullName.FormatName(user.Name);
            var efUser = _mapper.Map<EfUser>(user);
            efUser.Id = Guid.NewGuid();
            efUser.PasswordHash = PasswordHasher.Generate(user.Password);
            efUser.RegisteredAt = DateTime.UtcNow;

            _context.Add(efUser);
            _context.SaveChanges();
        }

        public async Task AddAsync(ModelUser user)
        {
            if (await _context.Users.AnyAsync(u => u.Login == user.Login || u.Email == user.Email))
            {
                throw new DataException("User with this login or email can't be created");
            }

            user.Email = user.Email.ToLower();
            FullName.FormatName(user.Name);
            var efUser = _mapper.Map<EfUser>(user);
            efUser.Id = Guid.NewGuid();
            efUser.PasswordHash = PasswordHasher.Generate(user.Password);
            efUser.RegisteredAt = DateTime.UtcNow;

            await _context.AddAsync(efUser);
            await _context.SaveChangesAsync();
        }

        public void Delete(Guid id)
        {
            var user = _context.Users.FirstOrDefault(u => u.Id == id);
            if (user == null)
            {
                throw new DataException("User not found");
            }

            _context.Users.Remove(user);
            _context.SaveChanges();
        }

        public async Task DeleteAsync(Guid id)
        {
            var user = await _context.Users.FirstOrDefaultAsync(u => u.Id == id);
            if (user == null)
            {
                throw new DataException("User not found");
            }

            _context.Users.Remove(user);
            await _context.SaveChangesAsync();
        }

        public ModelUser Get(Guid id)
        {
            var user = _context.Users.FirstOrDefault(u => u.Id == id);
            if (user == null)
            {
                throw new DataException("User not found");
            }

            return _mapper.Map<ModelUser>(user);
        }

        public ModelUser Get(Func<ModelUser, bool> predicate)
        {
            var user = _context.Users.Select(u => _mapper.Map<ModelUser>(u)).ToList().FirstOrDefault(predicate);
            if (user == null)
            {
                throw new DataException("User not found");
            }

            return _mapper.Map<ModelUser>(user);
        }

        public async Task<ModelUser> GetAsync(Guid id)
        {
            var user = await _context.Users.FirstOrDefaultAsync(u => u.Id == id);
            if (user == null)
            {
                throw new DataException("User not found");
            }

            return _mapper.Map<ModelUser>(user);
        }

        public async Task<ModelUser> GetAsync(Func<ModelUser, bool> predicate)
        {
            var user = (await _context.Users.Select(u => _mapper.Map<ModelUser>(u)).ToListAsync()).FirstOrDefault(predicate);
            if (user == null)
            {
                throw new DataException("User not found");
            }

            return _mapper.Map<ModelUser>(user);
        }

        public void Update(Guid id, ModelUser user)
        {
            var updatedUser = _context.Users.FirstOrDefault(u => u.Id == id);
            if (updatedUser == null)
            {
                throw new DataException("User not found");
            }

            var matchUsersCount = _context.Users.Where(u => u.Login == user.Login || u.Email == user.Email).Count();
            if (matchUsersCount > 1)
            {
                throw new DataException("This login or email can't be used");
            }

            UpdateFrom(updatedUser, user);
            _context.Users.Update(updatedUser);
            _context.SaveChanges();
        }

        public async Task UpdateAsync(Guid id, ModelUser user)
        {
            var updatedUser = await _context.Users.FirstOrDefaultAsync(u => u.Id == id);
            if (updatedUser == null)
            {
                throw new DataException("User not found");
            }

            var matchUsersCount = await _context.Users.Where(u => u.Login == user.Login || u.Email == user.Email).CountAsync();
            if (matchUsersCount > 1)
            {
                throw new DataException("This login or email can't be used");
            }

            UpdateFrom(updatedUser, user);
            _context.Users.Update(updatedUser);
            await _context.SaveChangesAsync();
        }

        private void UpdateFrom(EfUser destination, ModelUser source)
        {
            destination.Login = string.IsNullOrEmpty(source.Login) ? destination.Login : source.Login;
            destination.Email = string.IsNullOrEmpty(source.Email) ? destination.Email : source.Email;
            destination.FirstName = string.IsNullOrEmpty(source.Name.FirstName) ? destination.FirstName : source.Name.FirstName;
            destination.LastName = string.IsNullOrEmpty(source.Name.LastName) ? destination.LastName: source.Name.LastName;
            destination.PasswordHash = string.IsNullOrEmpty(source.Password) ? destination.PasswordHash : PasswordHasher.Generate(source.Password);
        }
    }
}
