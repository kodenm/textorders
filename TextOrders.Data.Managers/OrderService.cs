﻿using AutoMapper;
using System.Linq.Expressions;
using TextOrders.Data.Infrastructure;
using TextOrders.Data.Managers.Model;
using ModelOrder = TextOrders.Core.Models.Order;
using EfOrder = TextOrders.Data.Infrastructure.Models.Order;
using TextOrders.Core.Enums;
using Microsoft.EntityFrameworkCore;

namespace TextOrders.Data.Managers
{
    public class OrderService : IOrderService
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;

        public OrderService(DataContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public void Add(ModelOrder order)
        {
            var user = _context.Users.FirstOrDefault(u => u.Id == order.ClientId);
            if (user == null)
            {
                throw new DataException("User not found");
            }
            else if (user.Type == UserType.Writer.ToString())
            {
                throw new AccessViolationException("Order can't be placed by a writer");
            }

            if (string.IsNullOrEmpty(order.Subject.Trim()))
            {
                throw new DataException("Order can't be placed without a subject");
            }

            var createdOrder = new EfOrder();
            createdOrder.Id = Guid.NewGuid();
            createdOrder.Subject = order.Subject.Trim();
            createdOrder.Description = order.Description;
            createdOrder.Client = user;
            createdOrder.CreatedAt = DateTime.UtcNow;
            createdOrder.Status = OrderStatus.Awaiting.ToString();

            _context.Orders.Add(createdOrder);
            _context.SaveChanges();
        }

        public async Task AddAsync(ModelOrder order)
        {
            var user = await _context.Users.FirstOrDefaultAsync(u => u.Id == order.ClientId);
            if (user == null)
            {
                throw new DataException("User not found");
            }
            if (user.Type == UserType.Writer.ToString())
            {
                throw new AccessViolationException("Order can't be placed by a writer");
            }

            if (string.IsNullOrEmpty(order.Subject.Trim()) || string.IsNullOrEmpty(order.Description.Trim()))
            {
                throw new DataException("Order can't be placed without a subject and description");
            }

            var createdOrder = new EfOrder
            {
                Id = Guid.NewGuid(),
                Subject = order.Subject.Trim(),
                Description = order.Description,
                Client = user,
                CreatedAt = DateTime.UtcNow,
                Status = OrderStatus.Awaiting.ToString()
            };

            await _context.Orders.AddAsync(createdOrder);
            await _context.SaveChangesAsync();
        }

        public void Delete(Guid id)
        {
            var order = _context.Orders.FirstOrDefault(u => u.Id == id);
            if (order == null)
            {
                throw new DataException("Order not found");
            }

            _context.Orders.Remove(order);
            _context.SaveChanges();
        }

        public async Task DeleteAsync(Guid id)
        {
            var order = await _context.Orders.FirstOrDefaultAsync(u => u.Id == id);
            if (order == null)
            {
                throw new DataException("Order not found");
            }

            _context.Orders.Remove(order);
            await _context.SaveChangesAsync();
        }

        public ModelOrder Get(Guid id)
        {
            var order = _context.Orders.FirstOrDefault(o => o.Id == id);
            if (order == null)
            {
                throw new DataException("Order not found");
            }

            return _mapper.Map<ModelOrder>(order);
        }

        public ModelOrder Get(Func<ModelOrder, bool> predicate)
        {
            var order = _context.Orders.Select(o => _mapper.Map<ModelOrder>(o)).ToList().SingleOrDefault(predicate);
            if (order == null)
            {
                throw new DataException("Order not found");
            }

            return _mapper.Map<ModelOrder>(order);
        }

        public async Task<ModelOrder> GetAsync(Guid id)
        {
            var order = await _context.Orders.FirstOrDefaultAsync(o => o.Id == id);
            if (order == null)
            {
                throw new DataException("Order not found");
            }

            return _mapper.Map<ModelOrder>(order);
        }

        public async Task<ModelOrder> GetAsync(Func<ModelOrder, bool> predicate)
        {
            var order = (await _context.Orders.Select(o => _mapper.Map<ModelOrder>(o)).ToListAsync()).FirstOrDefault(predicate);
            if (order == null)
            {
                throw new DataException("Order not found");
            }

            return _mapper.Map<ModelOrder>(order);
        }

        public void Proceed(Guid orderId, Guid writerId)
        {
            var order = _context.Orders.FirstOrDefault(o => o.Id == orderId);
            if (order == null)
            {
                throw new DataException("Order not found");
            }

            var writer = _context.Users.FirstOrDefault(u => u.Id == writerId && u.Type == UserType.Writer.ToString());
            if (writer == null)
            {
                throw new AccessViolationException("Unauthorized");
            }

            if (order.Status == OrderStatus.Awaiting.ToString())
            {
                order.Writer = writer;
                order.Status = OrderStatus.InProgress.ToString();
                order.BeganAt = DateTime.UtcNow;
                order.FinishedAt = null;

                _context.Orders.Update(order);
                _context.SaveChanges();
            }

            throw new DataException("Can't change the order state");
        }

        public async Task ProceedAsync(Guid orderId, Guid writerId)
        {
            var order = await _context.Orders.FirstOrDefaultAsync(o => o.Id == orderId);
            if (order == null)
            {
                throw new DataException("Order not found");
            }

            var writer = await _context.Users.FirstOrDefaultAsync(u => u.Id == writerId && u.Type == UserType.Writer.ToString());
            if (writerId == order.ClientId)
            {
                throw new AccessViolationException("Client can't take its order");
            }

            if (order.Status == OrderStatus.Awaiting.ToString())
            {
                order.WriterId = writerId;
                order.Status = OrderStatus.InProgress.ToString();
                order.BeganAt = DateTime.UtcNow;
                order.FinishedAt = null;

                _context.Orders.Update(order);
                await _context.SaveChangesAsync();
            }
            else
            {
                throw new DataException("Order's state can't be changed");
            }
        }

        public void Review(Guid orderId)
        {
            var order = _context.Orders.FirstOrDefault(o => o.Id == orderId);
            if (order == null)
            {
                throw new DataException("Order not found");
            }

            if (order.Status == OrderStatus.InProgress.ToString())
            {
                order.Status = OrderStatus.OnReview.ToString();

                _context.Orders.Update(order);
                _context.SaveChanges();
            }

            throw new DataException("Order's state can't be changed");
        }

        public async Task ReviewAsync(Guid orderId)
        {
            var order = await _context.Orders.FirstOrDefaultAsync(o => o.Id == orderId);
            if (order == null)
            {
                throw new DataException("Order not found");
            }

            if (order.Status == OrderStatus.InProgress.ToString())
            {
                order.Status = OrderStatus.OnReview.ToString();

                _context.Orders.Update(order);
                await _context.SaveChangesAsync();
            }

            throw new DataException("Order is completed and can't be reviewed");
        }

        public void Complete(Guid orderId)
        {
            var order = _context.Orders.FirstOrDefault(o => o.Id == orderId);
            if (order == null)
            {
                throw new DataException("Order not found");
            }

            if (order.Status == OrderStatus.OnReview.ToString())
            {
                order.Status = OrderStatus.Completed.ToString();
                order.FinishedAt = DateTime.Now;
                _context.Orders.Update(order);
                _context.SaveChanges();
            }

            throw new DataException("Order can't be completed");
        }

        public async Task CompleteAsync(Guid orderId)
        {
            var order = await _context.Orders.FirstOrDefaultAsync(o => o.Id == orderId);
            if (order == null)
            {
                throw new DataException("Order not found");
            }

            if (order.Status == OrderStatus.OnReview.ToString())
            {
                order.Status = OrderStatus.Completed.ToString();
                order.FinishedAt = DateTime.Now;

                _context.Orders.Update(order);
                await _context.SaveChangesAsync();
            }

            throw new DataException("Order can't be completed");
        }

        public void PublishText(Guid orderId, string text)
        {
            var order = _context.Orders.FirstOrDefault(o => o.Id == orderId);
            if (order == null)
            {
                throw new DataException("Order not found");
            }

            if (OrderStatus.InProgress.ToString() == order.Status)
            {
                order.Status = OrderStatus.Completed.ToString();

                _context.Orders.Update(order);
                _context.SaveChanges();
            }

            throw new DataException("Can't publish now");
        }

        public async Task PublishTextAsync(Guid orderId, string text)
        {
            var order = await _context.Orders.FirstOrDefaultAsync(o => o.Id == orderId);
            if (order == null)
            {
                throw new DataException("Order not found");
            }

            if (OrderStatus.InProgress.ToString() == order.Status)
            {
                order.Status = OrderStatus.Completed.ToString();

                _context.Orders.Update(order);
                await _context.SaveChangesAsync();
            }

            throw new DataException("Can't publish now");
        }
    }
}
