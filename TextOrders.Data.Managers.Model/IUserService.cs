﻿using System.Linq.Expressions;
using TextOrders.Core.Models;

namespace TextOrders.Data.Managers.Model
{
    public interface IUserService
    {
        void Add(User user);
        Task AddAsync(User user);
        User Get(Guid id);
        Task<User> GetAsync(Guid id);
        User Get(Func<User, bool> predicate);
        Task<User> GetAsync(Func<User, bool> predicate);
        void Update(Guid id, User user);
        Task UpdateAsync(Guid id, User User);
        void Delete(Guid id);
        Task DeleteAsync(Guid id);
    }
}
