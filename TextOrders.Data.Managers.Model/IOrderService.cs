﻿using TextOrders.Core.Models;

namespace TextOrders.Data.Managers.Model
{
    public interface IOrderService
    {
        void Add(Order order);
        Task AddAsync(Order order);
        Order Get(Guid id);
        Task<Order> GetAsync(Guid id);
        Order Get(Func<Order, bool> predicate);
        Task<Order> GetAsync(Func<Order, bool> predicate);
        void Delete(Guid id);
        Task DeleteAsync(Guid id);
        void Proceed(Guid orderId, Guid writerId);
        Task ProceedAsync(Guid orderId, Guid writerId);
        void Review(Guid orderId);
        Task ReviewAsync(Guid orderId);
        void Complete(Guid orderId);
        Task CompleteAsync(Guid orderId);
        void PublishText(Guid orderId, string text);
        Task PublishTextAsync(Guid orderId, string text);
    }
}
